﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Sogeti.Demo.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Doing Service Call");

            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            var clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback += CheckServerTrust;
            X509Certificate2 certificate = store.Certificates[1];
            clientHandler.ClientCertificates.Add(certificate);
            HttpClient client = new HttpClient(clientHandler);
            
            string content = client.GetStringAsync(@"https://localhost:5443/api/values").GetAwaiter().GetResult();
            Console.WriteLine(content);

            Console.Read();
        }

        private static bool CheckServerTrust(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3, SslPolicyErrors arg4)
        {
            return true;
        }
    }
}