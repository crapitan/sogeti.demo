﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace Sogeti.Demo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("A quick SSL DEMO");

            var host = new WebHostBuilder()
                .UseKestrel(options =>
                {
                    options.UseHttps(@"DevelopmentCA.cer", "test");
                })
                .UseUrls("https://*:5443")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            host.Run();
        }
    }
}
