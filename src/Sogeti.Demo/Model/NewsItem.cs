﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sogeti.Demo.Model
{
    public class NewsItem
    {
        public string Title { get; set; }

        public string Lead { get; set; }

        public string Content { get; set; }

        public string ImageURI { get; set; }
    }
}
