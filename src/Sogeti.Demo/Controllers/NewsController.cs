﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Sogeti.Demo.Model;

namespace Sogeti.Demo.Controllers
{
    [Route("api/[controller]")]
    public class NewsController : Controller
    {
        [HttpGet]
        public IEnumerable<NewsItem> Get()
        {
            return new List<NewsItem>
            {
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
                new NewsItem { Title = "Sweden wins Hockey WM", Content = "The greatest country of all hockey nationns", ImageURI ="", Lead ="A total awsome team" },
            };
        }
    }
}
