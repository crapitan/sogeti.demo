﻿using System;
using System.IO;
using Java.Security;
using Javax.Net.Ssl;
using Java.Net;
using Java.Security.Cert;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sogeti.Demo.Mobile.Droid
{
    public class MyData : IData
    {
        private string url = "https://10.211.55.24:5443/api/";

        public async Task<T> GetAsync<T>(string path)
        {
           var content = await GetStringAsnyc(path);
           return JsonConvert.DeserializeObject<T>(content);
        }

        public Task<string> GetStringAsnyc(string serverpath = "values")
        {
                return 
             Task.Run<string>(() =>
             {

                 string response = "";
                 
                 var path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
                 var filename = Path.Combine(path.ToString(), "Client1Cert.pfx");
                 var ca = Path.Combine(path.ToString(), "DevelopmentCA.cer");

                 var keyStore = KeyStore.GetInstance("PKCS12");
                 var fis = new System.IO.FileStream(filename, FileMode.Open);
                 keyStore.Load(fis, "test".ToCharArray());

                 KeyManagerFactory kmf = KeyManagerFactory.GetInstance("X509");
                 kmf.Init(keyStore, "test".ToCharArray());
                 var keyManagers = kmf.GetKeyManagers();

                 CertificateFactory certificateFactory = CertificateFactory.GetInstance("X.509");
                 X509Certificate cert = (X509Certificate)certificateFactory.GenerateCertificate(new System.IO.FileStream(ca, FileMode.Open));
                 String alias = cert.SubjectX500Principal.Name;

                 KeyStore trustStore = KeyStore.GetInstance(KeyStore.DefaultType);
                 trustStore.Load(null);
                 trustStore.SetCertificateEntry(alias, cert);

                 TrustManagerFactory tmf = TrustManagerFactory.GetInstance("X509");
                 tmf.Init(trustStore);

                 var trustManagers = tmf.GetTrustManagers();
                 
                 SSLContext sslContext = SSLContext.GetInstance("TLS");
                 sslContext.Init(keyManagers, trustManagers, null);

                 String result = null;
                 HttpURLConnection urlConnection = null;

                 HttpsURLConnection.DefaultHostnameVerifier = new NullHostNameVerifier();

                 try
                 {
                     URL requestedUrl = new URL(url + serverpath);
                     urlConnection = (HttpURLConnection)requestedUrl.OpenConnection();

                     if (urlConnection is HttpsURLConnection)
                     {
                         ((HttpsURLConnection)urlConnection).SSLSocketFactory = sslContext.SocketFactory;
                     }

                     urlConnection.RequestMethod = "GET";
                     urlConnection.ConnectTimeout = 1500;
                     urlConnection.ReadTimeout = 1500;

                     var lastResponseCode = urlConnection.ResponseCode;
                     response = new StreamReader(urlConnection.InputStream).ReadToEnd();
                     var lastContentType = urlConnection.ContentType;
                 }
                 catch (Exception ex)
                 {
                     result = ex.ToString();
                 }
                 finally
                 {
                     if (urlConnection != null)
                     {
                         urlConnection.Disconnect();
                     }
                 }

                 return response;
             });
        }
    }
}