﻿using System;
using Javax.Net.Ssl;

namespace Sogeti.Demo.Mobile.Droid
{
    internal class NullHostNameVerifier : Java.Lang.Object, IHostnameVerifier
    {   
        public bool Verify(string hostname, ISSLSession session)
        {
           return true;
        }
    }
}