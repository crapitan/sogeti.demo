﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sogeti.Demo.Mobile
{
    public interface IData
    {
        Task<string> GetStringAsnyc(string path = "values");

        Task<T> GetAsync<T>(string path);
    }
}
